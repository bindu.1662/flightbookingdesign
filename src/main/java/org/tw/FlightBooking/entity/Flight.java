package org.tw.FlightBooking.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.tw.FlightBooking.models.TravelType;

import java.util.Map;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Flight {
    private int id;
    private String name;
    private String carrier;

    private Map<TravelType,TravelClass> travelClasses;

}
