package org.tw.FlightBooking.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.tw.FlightBooking.models.TravelType;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TravelClass {
    private int id;
    private int flightId;
    private TravelType travelType;
    private int totalSeats;
}
