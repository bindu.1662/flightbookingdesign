package org.tw.FlightBooking.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.tw.FlightBooking.models.TravelType;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class FlightTravelFare {
    int id;
    int scheduleId;
    TravelType travelType;
    int availableSeats;
    double baseFare;
}
