package org.tw.FlightBooking.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.tw.FlightBooking.models.TravelType;

import java.time.LocalDate;
import java.util.Map;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class FlightSchedule {
    private int id;
    private int flightId;
    private String source;
    private String destination;
    private LocalDate departureDateTime;

    private Map<TravelType,FlightTravelFare> flightTravelFares;
}

