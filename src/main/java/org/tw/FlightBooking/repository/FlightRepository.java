package org.tw.FlightBooking.repository;

import org.springframework.stereotype.Repository;
import org.tw.FlightBooking.entity.Flight;
import org.tw.FlightBooking.entity.FlightSchedule;
import org.tw.FlightBooking.entity.FlightTravelFare;
import org.tw.FlightBooking.entity.TravelClass;
import org.tw.FlightBooking.models.TravelType;

import java.time.LocalDate;
import java.util.*;

import static org.tw.FlightBooking.models.TravelType.*;

@Repository
public class FlightRepository {

    private List<Flight> flights = flightData();
    private List<FlightSchedule> flightSchedules = flightScheduleData();

    public List<FlightSchedule> getAllFlightSchedules()
    {
        return flightSchedules;
    }

    public Optional<Flight> getFlightById(int flightId) {
        return flights.stream().filter(flight -> flight.getId()==flightId).findFirst();
    }

    //In-memory database setup
    private List<Flight> flightData()
    {
        Map<TravelType, TravelClass> f1TravelClasses = new HashMap<>();
        Map<TravelType,TravelClass> f2TravelClasses = new HashMap<>();
        Map<TravelType,TravelClass> f3TravelClasses = new HashMap<>();

        TravelClass tc1 = new TravelClass(1,1,FIRST_CLASS,8);
        TravelClass tc2 = new TravelClass(2,1,BUSINESS_CLASS,35);
        TravelClass tc3 = new TravelClass(3,1,ECONOMY_CLASS,195);
        TravelClass tc4 = new TravelClass(4,2,ECONOMY_CLASS,144);
        TravelClass tc5 = new TravelClass(5,3,BUSINESS_CLASS,20);
        TravelClass tc6 = new TravelClass(6,3,ECONOMY_CLASS,152);

        f1TravelClasses.put(tc1.getTravelType(),tc1);
        f1TravelClasses.put(tc2.getTravelType(),tc2);
        f1TravelClasses.put(tc3.getTravelType(),tc3);

        f2TravelClasses.put(tc4.getTravelType(),tc4);

        f3TravelClasses.put(tc5.getTravelType(),tc5);
        f3TravelClasses.put(tc6.getTravelType(),tc6);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus A319 V2","Airbus",f2TravelClasses);
        Flight f3 = new Flight(3,"Airbus A321","Airbus",f3TravelClasses);

        List<Flight> flights = Arrays.asList(f1,f2,f3);
        return flights;
    }

    private List<FlightSchedule> flightScheduleData()
    {
        Map<TravelType, FlightTravelFare> fs1Fares = new HashMap<>();
        Map<TravelType,FlightTravelFare> fs2Fares = new HashMap<>();
        Map<TravelType,FlightTravelFare> fs3Fares = new HashMap<>();
        Map<TravelType,FlightTravelFare> fs4Fares = new HashMap<>();

        FlightTravelFare st1 = new FlightTravelFare(1,1, FIRST_CLASS,
                2,20000);
        FlightTravelFare st2 = new FlightTravelFare(2,1,BUSINESS_CLASS,
                3,13000);
        FlightTravelFare st3 = new FlightTravelFare(3,1,ECONOMY_CLASS,
                4,6000);
        FlightTravelFare st4 = new FlightTravelFare(4,2,ECONOMY_CLASS,
                5,4000);
        FlightTravelFare st5 = new FlightTravelFare(5,3,FIRST_CLASS,
                2,20000);
        FlightTravelFare st6 = new FlightTravelFare(6,3,BUSINESS_CLASS,
                2,10000);
        FlightTravelFare st7 = new FlightTravelFare(7,3,ECONOMY_CLASS,
                10,5000);
        FlightTravelFare st8 = new FlightTravelFare(8,4,BUSINESS_CLASS,
                2,10000);
        FlightTravelFare st9 = new FlightTravelFare(9,4,ECONOMY_CLASS,
                5,5000);

        fs1Fares.put(st1.getTravelType(),st1);
        fs1Fares.put(st2.getTravelType(),st2);
        fs1Fares.put(st3.getTravelType(),st3);

        fs2Fares.put(st4.getTravelType(),st4);

        fs3Fares.put(st5.getTravelType(),st5);
        fs3Fares.put(st6.getTravelType(),st6);
        fs3Fares.put(st7.getTravelType(),st7);

        fs4Fares.put(st8.getTravelType(),st8);
        fs4Fares.put(st9.getTravelType(),st9);


        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now(),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs2Fares);
        FlightSchedule fs3 = new FlightSchedule(3,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs3Fares);
        FlightSchedule fs4 = new FlightSchedule(4,3,"Visakhapatnam", "Bangalore",
                LocalDate.now(),fs4Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2,fs3,fs4);
        return flightSchedules;

    }
}
