package org.tw.FlightBooking.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tw.FlightBooking.entity.Flight;
import org.tw.FlightBooking.entity.FlightSchedule;
import org.tw.FlightBooking.entity.FlightTravelFare;
import org.tw.FlightBooking.models.FlightDataObject;
import org.tw.FlightBooking.models.FlightSearchCriteria;
import org.tw.FlightBooking.models.TravelType;
import org.tw.FlightBooking.repository.FlightRepository;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FlightService {

    private final FlightRepository flightRepository;

    @Autowired
    public FlightService(FlightRepository flightRepository) {
        this.flightRepository = flightRepository;
    }

    public Collection<FlightDataObject> searchFlights(FlightSearchCriteria flightSearchCriteria) {
        List<FlightSchedule> allFlightSchedules = flightRepository.getAllFlightSchedules();
        List<FlightSchedule> flightSchedules = allFlightSchedules.stream()
                .filter(schedule -> schedule.getSource().equals(flightSearchCriteria.getSource()) &&
                        schedule.getDestination().equals(flightSearchCriteria.getDestination()) &&
                        flightAvailableOnDate(schedule,flightSearchCriteria) &&
                        flightClassHasSeats(schedule,flightSearchCriteria))
                .collect(Collectors.toList());

        return flightSchedules.stream()
                .map(schedule -> convertToFlightDataObject(schedule,flightSearchCriteria))
                .collect(Collectors.toList());
    }

    private boolean flightAvailableOnDate(FlightSchedule schedule, FlightSearchCriteria criteria)
    {
        if(schedule.getDepartureDateTime().isEqual(criteria.getDepartureDateValue()))
        {
            if(criteria.getTravelTypeValue() == TravelType.FIRST_CLASS)
            {
                return Period.between(LocalDate.now(), criteria.getDepartureDateValue()).getDays() < 10
                        && Period.between(LocalDate.now(), criteria.getDepartureDateValue()).getDays() >= 0;
            }
            else if(criteria.getTravelTypeValue() == TravelType.BUSINESS_CLASS)
            {
                return Period.between(LocalDate.now(), criteria.getDepartureDateValue()).getDays() <= 28
                        && Period.between(LocalDate.now(), criteria.getDepartureDateValue()).getDays() >= 0;
            }
            else {
                return true;
            }
        }
        return false;
    }

    private FlightDataObject convertToFlightDataObject(FlightSchedule schedule, FlightSearchCriteria flightSearchCriteria) {
        Flight flight = flightRepository.getFlightById(schedule.getFlightId()).orElse(null);
        return FlightDataObject.builder()
                .id(flight.getId())
                .name(flight.getName())
                .origin(flightSearchCriteria.getSource())
                .destination(flightSearchCriteria.getDestination())
                .departureDate(flightSearchCriteria.getDepartureDateValue().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)))
                .travelType(flightSearchCriteria.getTravelTypeValue())
                .totalFare(calculateTotalFare(flight, schedule,flightSearchCriteria))
                .build();
    }

    private double calculateTotalFare(Flight flight, FlightSchedule schedule, FlightSearchCriteria criteria) {
        if(criteria.getTravelTypeValue() == TravelType.FIRST_CLASS) {
            return calculateFirstTotalFare(flight, schedule, criteria);
        } else if(criteria.getTravelTypeValue() == TravelType.BUSINESS_CLASS) {
            return calculateBusinessTotalFare(flight, schedule, criteria);
        } else {
            return calculateEconomyTotalFare(flight, schedule, criteria);
        }
    }

    private double calculateBusinessTotalFare(Flight flight, FlightSchedule schedule, FlightSearchCriteria criteria) {
        FlightTravelFare travelFare = schedule.getFlightTravelFares().get(criteria.getTravelTypeValue());

        double baseFare = travelFare.getBaseFare();
        double extra = 0;
        LocalDate departureDate = criteria.getDepartureDateValue();
        DayOfWeek dayOfWeek = departureDate.getDayOfWeek();
        if(dayOfWeek == DayOfWeek.MONDAY || dayOfWeek==DayOfWeek.FRIDAY || dayOfWeek==DayOfWeek.SUNDAY){
            extra = 0.4 * baseFare;
        }
        return ((baseFare+extra)*criteria.getPassengerCountValue());
    }

    private double calculateFirstTotalFare(Flight flight, FlightSchedule schedule,
                                           FlightSearchCriteria criteria) {
        FlightTravelFare travelFare = schedule.getFlightTravelFares().get(criteria.getTravelTypeValue());

        double baseFare = travelFare.getBaseFare();

        //Everyday 10% extra of base price
        int noOfDays = Period.between(LocalDate.now(), criteria.getDepartureDateValue()).getDays();
        double newBasePrice = baseFare + ((10-noOfDays) * 0.1 * baseFare);
        return newBasePrice * criteria.getPassengerCountValue();
    }

    private double calculateEconomyTotalFare(Flight flight, FlightSchedule schedule, FlightSearchCriteria criteria) {
        FlightTravelFare travelFare = schedule.getFlightTravelFares().get(criteria.getTravelTypeValue());
        int totalSeats = flight.getTravelClasses().get(criteria.getTravelTypeValue()).getTotalSeats();
        int availableSeats = travelFare.getAvailableSeats();
        double baseFare = travelFare.getBaseFare();
        double extra=0;

        int first40 = (int)(totalSeats-(totalSeats*0.4));
        int next50 = (int)((totalSeats) -(totalSeats*0.5));

        if(availableSeats>=first40 && availableSeats<=totalSeats)
        {
            extra = 0;
        }
        if(availableSeats<(first40) && availableSeats>=next50 )
        {
            extra = baseFare*0.3;
        }
        if(availableSeats<(next50) && availableSeats>=1)
        {
            extra = baseFare*0.6;
        }

        return ((baseFare+extra)*criteria.getPassengerCountValue());
    }


    private boolean flightClassHasSeats(FlightSchedule schedule, FlightSearchCriteria flightSearchCriteria) {
        FlightTravelFare flightTravelFare = schedule.getFlightTravelFares()
                .get(flightSearchCriteria.getTravelTypeValue());
        return flightTravelFare != null && flightTravelFare.getAvailableSeats() >=
                flightSearchCriteria.getPassengerCountValue();
    }
}

