package org.tw.FlightBooking.models;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FlightDataObject {
    private int id;
    private String name;
    private String origin;
    private String destination;
    private String departureDate;
    private TravelType travelType;
    private double totalFare;
}

