package org.tw.FlightBooking.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Optional;

import static org.tw.FlightBooking.models.TravelType.ECONOMY_CLASS;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FlightSearchCriteria {
    private String source;
    private String destination;

    private Optional<Integer> passengerCount;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Optional<LocalDate> departureDate;

    private Optional<TravelType> travelType;

    public Integer getPassengerCountValue()
    {
        return getPassengerCount().orElse(1);
    }

    public LocalDate getDepartureDateValue()
    {
        return getDepartureDate().orElse(LocalDate.now().plusDays(1));
    }

    public TravelType getTravelTypeValue()
    {
        return getTravelType().orElse(ECONOMY_CLASS);
    }

}

