package org.tw.FlightBooking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tw.FlightBooking.models.FlightDataObject;
import org.tw.FlightBooking.models.FlightSearchCriteria;
import org.tw.FlightBooking.services.FlightService;

import java.util.Collection;

@RestController
@RequestMapping("/api/flights")
public class FlightController {

    private FlightService flightService;

    @Autowired
    public FlightController(FlightService flightService) {
        this.flightService = flightService;
    }

    @GetMapping("/search")
    public Collection<FlightDataObject> searchFlights(FlightSearchCriteria flightSearchCriteria)
    {
        return flightService.searchFlights(flightSearchCriteria);
    }
}

