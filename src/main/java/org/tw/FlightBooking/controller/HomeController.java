package org.tw.FlightBooking.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/")
    public String home() {
        return "redirect:/flights";
    }

    @GetMapping("/flights")
    public String index() {
        return "flights";
    }
}
