package org.tw.FlightBooking.services;

import org.tw.FlightBooking.entity.Flight;
import org.tw.FlightBooking.entity.FlightSchedule;
import org.tw.FlightBooking.entity.FlightTravelFare;
import org.tw.FlightBooking.entity.TravelClass;
import org.tw.FlightBooking.models.FlightDataObject;
import org.tw.FlightBooking.models.FlightSearchCriteria;
import org.tw.FlightBooking.models.TravelType;
import org.tw.FlightBooking.repository.FlightRepository;
import org.junit.Before;
import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import static java.time.temporal.TemporalAdjusters.nextOrSame;
import static org.assertj.core.api.Assertions.assertThat;
import java.util.*;
import static org.tw.FlightBooking.models.TravelType.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class FlightServiceTest {

    private FlightRepository flightRepository;

    private FlightService flightService;

    @Before
    public void setUp() {
        flightRepository = mock(FlightRepository.class);
        flightService = new FlightService(flightRepository);
    }

    @Test
    public void shouldReturnFlightsMatchingSourceAndDestination() {
        // given

        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,1000,
                5,5000,2,2000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,1000,
                5,5000,5,2000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now(),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now(),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,30);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam", "Bangalore",
                Optional.of(3), Optional.of(LocalDate.now()), Optional.of(ECONOMY_CLASS));
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(1);
        assertThat(result).extracting("id").containsExactly(2);
    }

    @Test
    public void shouldReturnZeroFlightsForNoMatchingSourceAndDestination() {
        // given

        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,1000,
                5,5000,2,2000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,1000,
                5,5000,5,2000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now(),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Pune",
                LocalDate.now(),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,30);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Delhi",
                "Bangalore",
                Optional.empty(),
                Optional.empty(),
                Optional.empty());
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(0);
    }

    @Test
    public void shouldReturnFlightsMatchingNumberOfPassengers() {
        // given

        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,1000,
                5,5000,2,2000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,1000,
                5,5000,5,2000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,30);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore", Optional.of(5), Optional.empty(), Optional.empty());
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(1);
        assertThat(result).extracting("id").containsExactly(2);
    }


    @Test
    public void shouldReturnZeroFlightsWhenNoMatchingNumberOfPassengers() {
        // given

        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,1000,
                5,5000,2,2000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,1000,
                5,5000,5,2000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,30);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.of(6),
                Optional.empty(),
                Optional.empty());
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(0);
    }


    @Test
    public void shouldReturnFlightsForOneSeatWhenNumberOfPassengersIsEmpty() {
        // given

        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,1000,
                5,5000,2,2000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,1000,
                5,5000,5,2000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,30);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.empty(),
                Optional.empty(),
                Optional.empty());
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(2);
        assertThat(result).extracting("id").containsExactly(1,2);
    }


    @Test
    public void shouldReturnAllFlightsWhenTravelClassMatch() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,1000,
                5,5000,2,2000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,1000,
                1,5000,5,2000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,30);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.empty(),
                Optional.empty(),
                Optional.of(BUSINESS_CLASS));
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(2);
        assertThat(result).extracting("id").containsExactly(1,2);
    }

    @Test
    public void shouldReturnZeroFlightsWhenTravelClassNotMatches() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,0,0,
                5,5000,2,2000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,0,0,
                1,5000,5,2000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,0,20,30);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,0,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.empty(),
                Optional.empty(),
                Optional.of(FIRST_CLASS));
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(0);
    }

    @Test
    public void shouldReturnFlightsWithDefaultEconomyWhenTravelClassIsEmpty() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,1000,
                5,5000,2,2000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,1000,
                1,5000,5,2000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,30);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.empty(),
                Optional.empty(),
                Optional.empty());
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(2);
        assertThat(result).extracting("id").containsExactly(1,2);
    }

    @Test
    public void shouldReturnFlightsWhenTravelDateMatches() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,1000,
                5,5000,2,2000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,1000,
                1,5000,5,2000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,30);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.empty(),
                Optional.of(LocalDate.now().plusDays(1)),
                Optional.empty());
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(2);
        assertThat(result).extracting("id").containsExactly(1,2);
    }

    @Test
    public void shouldReturnZeroFlightsWhenTravelDateNotMatches() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,1000,
                5,5000,2,2000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,1000,
                1,5000,5,2000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,30);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.empty(),
                Optional.of(LocalDate.now()),
                Optional.empty());
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(0);
    }

    @Test
    public void shouldReturnFlightsWithDefaultNextDayDateWhenTravelDateIsEmpty() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,1000,
                5,5000,2,2000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,1000,
                1,5000,5,2000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now(),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,30);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.empty(),
                Optional.of(LocalDate.now().plusDays(1)),
                Optional.empty());
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(1);
        assertThat(result).extracting("id").containsExactly(2);
    }

    @Test
    public void shouldReturnFlightsDetailsWithCalculatedFareConsideringAllSeatsAsAvailable() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,5000,
                20,3000,30,1000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,5000,
                20,3000,30,1000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now(),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,30);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.of(2),
                Optional.empty(),
                Optional.empty());
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(1);
        assertThat(result).extracting("id").containsExactly(2);
        assertThat(result).extracting("totalFare").containsExactly(2000.0);
    }

    @Test
    public void shouldReturnCalculatedFareForEconomyClassBasedOnFirst40PercentOfSeats() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,15000,
                20,10000,180,6000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,16000,
                20,12000,30,5000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now(),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,195);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.of(2),
                Optional.empty(),
                Optional.of(ECONOMY_CLASS));
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(1);
        assertThat(result).extracting("id").containsExactly(1);
        assertThat(result).extracting("totalFare").containsExactly(12000.0);
    }

    @Test
    public void shouldReturnCalculatedFareForEconomyClassBasedOnNext50PercentOfSeats() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,5000,
                20,3000,100,6000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,5000,
                20,3000,30,1000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now(),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,195);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.of(2),
                Optional.empty(),
                Optional.of(ECONOMY_CLASS));
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(1);
        assertThat(result).extracting("id").containsExactly(1);
        assertThat(result).extracting("totalFare").containsExactly(15600.0);
    }

    @Test
    public void shouldReturnCalculatedFareForEconomyClassBasedOnLast10PercentOfSeats() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,5000,
                20,3000,22,6000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,5000,
                20,3000,30,1000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(1),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now(),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,195);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.of(2),
                Optional.empty(),
                Optional.of(ECONOMY_CLASS));
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(1);
        assertThat(result).extracting("id").containsExactly(1);
        assertThat(result).extracting("totalFare").containsExactly(19200.0);
    }

    @Test
    public void shouldReturnCalculatedFareForBusinessClassBasedOnDayOfTheWeekAsFriday() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,5000,
                10,3000,22,6000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,5000,
                15,3000,30,1000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().with(nextOrSame(DayOfWeek.FRIDAY)),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.of(2019,8,24),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,195);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.of(2),
                // Optional.of(LocalDate.of(2019,8,30)),
                Optional.of(LocalDate.now().with(nextOrSame(DayOfWeek.FRIDAY))),
                Optional.of(BUSINESS_CLASS));
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(1);
        assertThat(result).extracting("id").containsExactly(1);
        assertThat(result).extracting("totalFare").containsExactly(8400.0);
    }

    @Test
    public void shouldReturnCalculatedFareForBusinessClassBasedOnDayOfTheWeekAsSunday() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,5000,
                10,3000,22,6000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,5000,
                15,3000,30,1000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().with(nextOrSame(DayOfWeek.SUNDAY)),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now(),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,195);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.of(2),
                Optional.of(LocalDate.now().with(nextOrSame(DayOfWeek.SUNDAY))),
                Optional.of(BUSINESS_CLASS));
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(1);
        assertThat(result).extracting("id").containsExactly(1);
        assertThat(result).extracting("totalFare").containsExactly(8400.0);
    }

    @Test
    public void shouldReturnCalculatedFareForBusinessClassBasedOnDayOfTheWeekAsMonday() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,5000,
                10,3000,22,6000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,5000,
                15,3000,30,1000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().with(nextOrSame(DayOfWeek.MONDAY)),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now().with(nextOrSame(DayOfWeek.TUESDAY)),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,195);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.of(2),
                Optional.of(LocalDate.now().with(nextOrSame(DayOfWeek.MONDAY))),
                Optional.of(BUSINESS_CLASS));
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(1);
        assertThat(result).extracting("id").containsExactly(1);
        assertThat(result).extracting("totalFare").containsExactly(8400.0);
    }

    @Test
    public void shouldReturnCalculatedFareForBusinessClassForDayOtherThanMondayFridayAndSunday() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,5000,
                10,3000,22,6000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,5000,
                15,3000,30,1000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().with(nextOrSame(DayOfWeek.SATURDAY)),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now(),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,195);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.of(2),
                Optional.of(LocalDate.now().with(nextOrSame(DayOfWeek.SATURDAY))),
                Optional.of(BUSINESS_CLASS));
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(1);
        assertThat(result).extracting("id").containsExactly(1);
        assertThat(result).extracting("totalFare").containsExactly(6000.0);
    }

    @Test
    public void shouldReturnZeroBusinessClassFlightsWhenBooked4WeeksBeforeDepartureDate() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,5000,
                10,3000,22,6000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,5000,
                15,3000,30,1000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.of(2019,9,30),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now(),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,195);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.of(2),
                Optional.of(LocalDate.of(2019,8,30)),
                Optional.of(BUSINESS_CLASS));
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(0);
    }

    @Test
    public void shouldReturnCalculatedFareForFirstClassOnBooking3DaysBeforeDepartureDate() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,5000,
                10,3000,22,6000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,5000,
                15,3000,30,1000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.of(2019,8,26),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(3),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,195);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.of(2),
                Optional.of(LocalDate.now().plusDays(3)),
                Optional.of(FIRST_CLASS));
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(1);
        assertThat(result).extracting("id").containsExactly(2);
        assertThat(result).extracting("totalFare").containsExactly(17000.0);
    }

    @Test
    public void shouldReturnCalculatedFareForFirstClassOnBookingExactly10DaysBeforeDepartureDate() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,5000,
                10,3000,22,6000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,5000,
                15,3000,30,1000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(9),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now(),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,195);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.of(2),
                Optional.of(LocalDate.now().plusDays(9)),
                Optional.of(FIRST_CLASS));
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(1);
        assertThat(result).extracting("id").containsExactly(1);
        assertThat(result).extracting("totalFare").containsExactly(11000.0);
    }

    @Test
    public void shouldReturnZeroFirstClassFlightsOnBookingMoreThan10DaysBeforeDepartureDate() {
        Map<TravelType, FlightTravelFare> fs1Fares = buildFlightFares(
                1,10,5000,
                10,3000,22,6000);
        Map<TravelType, FlightTravelFare> fs2Fares = buildFlightFares(
                2,10,5000,
                15,3000,30,1000);

        FlightSchedule fs1 = new FlightSchedule(1,1,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(11),fs1Fares);
        FlightSchedule fs2 = new FlightSchedule(2,2,"Visakhapatnam", "Bangalore",
                LocalDate.now().plusDays(11),fs2Fares);

        List<FlightSchedule> flightSchedules = Arrays.asList(fs1,fs2);

        Map<TravelType, TravelClass> f1TravelClasses = buildTravelClasses(
                1,10,20,195);
        Map<TravelType, TravelClass> f2TravelClasses = buildTravelClasses(
                2,10,20,30);

        Flight f1 = new Flight(1,"Boeing 777- 200LR(77L)","Boeing",f1TravelClasses);
        Flight f2 = new Flight(2,"Airbus","Airbus",f2TravelClasses);

        when(flightRepository.getFlightById(fs1.getFlightId())).thenReturn(Optional.of(f1));
        when(flightRepository.getFlightById(fs2.getFlightId())).thenReturn(Optional.of(f2));
        when(flightRepository.getAllFlightSchedules()).thenReturn(flightSchedules);


        //when
        FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria("Visakhapatnam",
                "Bangalore",
                Optional.of(2),
                Optional.of(LocalDate.now().plusDays(11)),
                Optional.of(FIRST_CLASS));
        Collection<FlightDataObject> result = flightService.searchFlights(flightSearchCriteria);

        //then
        assertThat(result).hasSize(0);
    }

    private Map<TravelType, FlightTravelFare> buildFlightFares(
            int scheduleId,int fcAvailableSeats,double fcBaseFare,
            int bcAvailableSeats,double bcBaseFare,
            int ecAvailableSeats,double ecBaseFare
    )
    {
        Map<TravelType, FlightTravelFare> fs1Fares = new HashMap<>();

        FlightTravelFare st1 = new FlightTravelFare(1,scheduleId, FIRST_CLASS,
                fcAvailableSeats,fcBaseFare);
        FlightTravelFare st2 = new FlightTravelFare(2,scheduleId,BUSINESS_CLASS,
                bcAvailableSeats,bcBaseFare);
        FlightTravelFare st3 = new FlightTravelFare(3,scheduleId,ECONOMY_CLASS,
                ecAvailableSeats,ecBaseFare);

        fs1Fares.put(st1.getTravelType(),st1);
        fs1Fares.put(st2.getTravelType(),st2);
        fs1Fares.put(st3.getTravelType(),st3);

        return fs1Fares;
    }

    private Map<TravelType, TravelClass> buildTravelClasses(
            int flightId,int fcTotalSeats, int bcTotalSeats, int ecTotalSeats)
    {
        Map<TravelType, TravelClass> f1TravelClasses = new HashMap<>();

        TravelClass tc1 = new TravelClass(1,flightId,FIRST_CLASS,fcTotalSeats);
        TravelClass tc2 = new TravelClass(2,flightId,BUSINESS_CLASS,bcTotalSeats);
        TravelClass tc3 = new TravelClass(3,flightId,ECONOMY_CLASS,ecTotalSeats);

        f1TravelClasses.put(tc1.getTravelType(),tc1);
        f1TravelClasses.put(tc2.getTravelType(),tc2);
        f1TravelClasses.put(tc3.getTravelType(),tc3);

        return f1TravelClasses;
    }
}

