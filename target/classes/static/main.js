$(document).ready(function () {
    $("#flightsTable").hide();
    $("#search-form").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        fire_ajax_submit();

    });

});

function makeTable(data) {
    var rows = '';
    $.each(data, function(rowIndex, r) {
        var row = "<tr>";
        row += "<td>"+r.name+"</td>";
        row += "<td>"+r.origin+"</td>";
        row += "<td>"+r.destination+"</td>";
        row += "<td>"+r.departureDate+"</td>";
        row += "<td>"+r.travelType+"</td>";
        row += "<td>"+r.totalFare+"</td>";
        row += "</tr>";
        rows += row;
    });
    return rows;
}

function fire_ajax_submit() {
    $.ajax({
        type: "GET",
        url: "/api/flights/search",
        data: {
            source: $("#source").val(),
            destination: $("#destination").val(),
            passengerCount: $("#passengerCount").val(),
            departureDate: $("#departureDate").val(),
            travelType: $("#travelType").val()
        },
        dataType: 'json',
        timeout: 800000,
        success: function (response) {
            console.log("SUCCESS : ", response);
            $("#flightsTable").show();
            if(response!=null && response.length>0)
            {
                var rows = makeTable(response);
                $('#flightRows').html(rows);
            }
            else
            {
                $("#flightRows").text('No flights found.');
            }
            var json = "<h4>Ajax Response</h4><pre>"+ JSON.stringify(response, null, 4) + "</pre>";
            //$('#feedback').html(json);
        },
        error: function (e) {
            console.log("ERROR : ", e);
            var json = "<h4>Ajax Response</h4><pre>"+ e.responseText + "</pre>";
            $('#feedback').html(json);
        }
    });

}

